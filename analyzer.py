import endpoints
import schedule, time

from datetime import datetime
from Binance import client
from data_collector import DataCollector


class Analyzer:
    def __init__(self):
        self.data = DataCollector()

        self.take = endpoints.TAKE
        self.take2 = endpoints.TAKE2
        self.stop = endpoints.STOP

        self.client = 0
        # self.client = client.Client()
        self.signal = None
        self.next_time = 0

        self.counter_long = 0
        self.counter_short = 0

        self.flag = 0
        self.history_signals = []

    def get_signal(self, row):
        row = next(row.iterrows())
        index = row[0]
        data = row[1]

        amount = self.client.balance/data.close*endpoints.QUANTITY*endpoints.LEVERAGE
        amount = int(amount)
        first_take_amount = int(amount/2)

        # long position
        if data.high >= data.pc_upper and data.K < 85 and data.close >= data.MA54:
            first_take = data.close * (1+self.take2)
            second_take = data.close * (1+self.take)

            first_take = "{price:0.{precision}f}".format(price=first_take, precision=6)
            second_take = "{price:0.{precision}f}".format(price=second_take, precision=6)

            enter = "{price:0.{precision}f}".format(price=data.close*1.001, precision=6)

            stop = data.close * (1 - self.stop)
            stop = "{price:0.{precision}f}".format(price=stop, precision=6)

            signal = {
                "index": index,
                "time": data.time,
                "enter": enter,
                "first_take": first_take,
                "second_take": second_take,
                "stop": stop,
                "amount": amount,
                "first_take_amount": first_take_amount,
                "trade": 'long',
                "K": data.K,
                "D": data.D
            }

            self.counter_long += 1
            self.counter_short = 0

            return signal

        # short position
        elif data.low <= data.pc_lower and data.K > 30 and data.close <= data.MA54:
            first_take = data.close * (1 - self.take2)
            second_take = data.close * (1 - self.take)

            first_take = "{price:0.{precision}f}".format(price=first_take, precision=6)
            second_take = "{price:0.{precision}f}".format(price=second_take, precision=6)

            enter = "{price:0.{precision}f}".format(price=data.close * 0.999, precision=6)

            stop = data.close * (1 + self.stop)
            stop = "{price:0.{precision}f}".format(price=stop, precision=6)

            signal = {
                "index": index,
                "time": data.time,
                "enter": enter,
                "first_take": first_take,
                "second_take": second_take,
                "stop": stop,
                "amount": amount,
                "first_take_amount": first_take_amount,
                "trade": 'short',
                "K": data.K,
                "D": data.D
            }

            self.counter_long = 0
            self.counter_short += 1

            return signal

        return False

    def get_signal_hist(self, row):
        index = row[0]
        data = row[1]

        # long position
        # if data.high == data.pc_upper and data.K < 80:
        # if data.high >= data.pc_upper and data.K < 85:
        # if data.high >= data.pc_upper and data.K < 85 and data.open > data.MA54 and self.counter_long <= 3:
        if data.high >= data.pc_upper and data.K < 85 and data.open > data.MA54:
        # if data.high >= data.pc_upper and data.K <= 85 and data.MA21 < data.MA14 < data.MA7:
        # if data.high >= data.pc_upper and data.close > data.MA21:
        # if (data.high >= data.pc_upper or data.low <= data.pc_lower) and data.close > data.MA34:
        # if data.open < data.MA55 < data.close:
        # if data.high >= data.pc_upper and data.close > data.MA7 and self.prev_trade == "short" or not self.prev_trade:
            # stop = 0
            # if data.open > data.close:
            #     stop = data.close*(1 - self.stop)
            # else:
            #     stop = data.open*(1 - endpoints.STOP_2)
            take = data.close*(1 + self.take)
            take2 = data.close*(1 + self.take2)
            stop = data.close*(1 - self.stop)

            # take = 0

            # stop = data.open * (1 - endpoints.STOP_2)

            signal = {
                "index": index,
                "time": data.time,
                "enter": data.close,
                "stop": stop,
                # "stop": data.pc_lower*(1 + self.stop),
                "take": take,
                "take2": take2,
                "trade": 'long',
                "K": data.K,
                "D": data.D,
                "Flag": 0,
                "Profit": 0
            }
            self.counter_long += 1
            self.counter_short = 0
            return signal
        # elif data.low <= data.pc_lower and data.K > 15:
        # elif data.low <= data.pc_lower and data.K > 30 and data.open < data.MA54 and self.counter_short <= 3:
        elif data.low <= data.pc_lower and data.K > 30 and data.open < data.MA54:
        # elif data.low <= data.pc_lower and data.K >= 15 and data.MA21 > data.MA14 > data.MA7:
        # elif data.low <= data.pc_lower and data.close < data.MA21:
        # elif (data.low <= data.pc_lower or data.high >= data.pc_upper) and data.close < data.MA34:
        # elif data.low <= data.pc_lower and data.close < data.MA7 and self.prev_trade == "long" or not self.prev_trade:
        # elif data.open > data.MA55 > data.close:
            # stop = 0
            # if data.open > data.close:
            #     stop = data.open * (1 + endpoints.STOP_2)
            # else:
            #     stop = data.close * (1 + self.stop)
            # take = 0

            take = data.close * (1 - self.take)
            take2 = data.close * (1 - self.take2)
            stop = data.close * (1 + self.stop)

            # stop = data.open * (1 + endpoints.STOP_2)

            signal = {
                "index": index,
                "time": data.time,
                "enter": data.close,
                "stop": stop,
                # "stop": data.pc_upper*(1 - self.stop),
                "take": take,
                "take2": take2,
                "trade": 'short',
                "K": data.K,
                "D": data.D,
                "Flag": 0,
                "Profit": 0
            }
            self.counter_long = 0
            self.counter_short += 1
            return signal
        else:
            self.signal = 0

    def ping(self):
        self.client.client.keep_user_data_stream()

    def rebase_sched(self):
        orders = self.client.client.get_open_orders(endpoints.SYMBOL)

        orders_id = []

        for order in orders:
            orders_id.append(order.clientOrderId)

        if "first" not in orders_id and self.flag == 0:
            if self.client.rebase_stop(self.signal):
                self.flag = 1

    def get_current_signal(self):
        time_now = datetime.now()

        if time_now.second < 50:
            time.sleep(5)

        print(datetime.now())

        print("Start_Get_signal")
        self.data.get_data()
        last_row = self.data.df.tail(1)
        signal = self.get_signal(last_row)

        if signal:
            if self.client.trade(signal):
                self.signal = signal
                self.data.df = 0
                self.flag = 0

        print(datetime.now())
        print("End_Get_signal")
        return self.signal

    def print_signal(self):
        if self.signal:
            print(
                "Time open: " + str(datetime.fromtimestamp(float(self.signal['time']/1000))) +
                "Price open: " + str(self.signal['enter']) +
                "Take: " + str(self.signal['take']) +
                "Stop: " + str(self.signal['stop'])
            )

    def get_next_time(self):
        self.data.get_data()
        delimiter = float(self.data.df.tail(1).time) - float(self.data.df.tail(1).time) + 1

        self.next_time = datetime.fromtimestamp(
            (float(self.data.df.tail(1).time_close) + delimiter)/1000
        )

        return self.next_time


if __name__ == '__main__':
    analyzer = Analyzer()
    time_del = analyzer.get_next_time() - datetime.now()

    print(analyzer.get_next_time())
    print(time_del + datetime.now())

    time_del = int(time_del.seconds) + int(time_del.microseconds) / 1000000
    time.sleep(int(time_del))
    schedule.every(295).seconds.do(analyzer.get_current_signal)
    schedule.every(60).seconds.do(analyzer.rebase_sched)
    schedule.every(40).minutes.do(analyzer.ping)

    print("Start script: " + str(datetime.now()))
    while True:
        schedule.run_pending()
