import sys
sys.path.append("..")

from endpoints import *
from binance_f.requestclient import RequestClient
from binance_f.constant.test import *
from binance_f.base.printobject import *
from binance_f.model.constant import *


class Client:
    def __init__(self):
        self.client = RequestClient(
            api_key=API_KEY,
            secret_key=SECRET_KEY,
            url=BASE
        )

        self.market = None
        self.first_take = None
        self.second_take = None
        self.stop = None

        self.active_position = None
        self.signal = {}

        try:
            self.client.change_initial_leverage(symbol=SYMBOL, leverage=LEVERAGE)
            self.client.change_margin_type(symbol=SYMBOL, marginType=FuturesMarginType.ISOLATED)
        except Exception as e:
            print(e)

        self.balance = self.client.get_balance()[0].balance

    def update_balance(self):
        self.balance = self.client.get_balance()[0].balance

    def update_position(self):
        self.active_position = self.client.get_position()

    def validator(self):
        self.update_position()
        for position in self.active_position:
            if position.positionAmt:
                return False

        try:
            self.client.cancel_all_orders(symbol=SYMBOL)
        except Exception as e:
            print(e)

        self.update_balance()

        return True

    def trade(self, signal):
        print(signal['amount'])
        if self.validator():
            if signal['trade'] == "long":
                self.signal = signal
                # MARKET_LONG
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.MARKET,
                    quantity=signal['amount'],
                    newClientOrderId="enter"
                )
                # FIRST_TAKE_LONG
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.TAKE_PROFIT_MARKET,
                    stopPrice=signal['first_take'],
                    quantity=signal['first_take_amount'],
                    newClientOrderId="first"
                )
                # SECOND_TAKE_LONG
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.TAKE_PROFIT_MARKET,
                    stopPrice=signal['second_take'],
                    closePosition=True,
                    newClientOrderId="second"
                )

                #STOP_LONG
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.STOP_MARKET,
                    stopPrice=signal['stop'],
                    newClientOrderId="stop",
                    closePosition=True
                )

                return True

            if signal['trade'] == "short":
                self.signal = signal
                #MARKET_SHORT
                self.market = self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.SELL,
                    ordertype=OrderType.MARKET,
                    quantity=signal['amount'],
                    newClientOrderId="market"
                )

                #FIRST_TAKE_SHORT
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.TAKE_PROFIT_MARKET,
                    stopPrice=signal['first_take'],
                    quantity=signal['first_take_amount'],
                    newClientOrderId="first"
                )
                #SECOND_TAKE_SHORT
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.TAKE_PROFIT_MARKET,
                    stopPrice=signal['second_take'],
                    closePosition=True,
                    newClientOrderId="second"
                )

                #STOP_SHORT
                self.client.post_order(
                    symbol=SYMBOL,
                    side=OrderSide.BUY,
                    ordertype=OrderType.STOP_MARKET,
                    stopPrice=signal['stop'],
                    newClientOrderId="stop",
                    closePosition=True
                )

                return True
            return False

    def rebase_stop(self, signal):
        if not self.validator():
            try:
                if signal['trade'] == 'long':
                    # STOP_LONG
                    self.client.post_order(
                        symbol=SYMBOL,
                        side=OrderSide.SELL,
                        ordertype=OrderType.STOP_MARKET,
                        stopPrice=signal['enter'],
                        newClientOrderId="new_stop",
                        closePosition=True
                    )
                if signal['trade'] == 'short':
                    # STOP_LONG
                    self.client.post_order(
                        symbol=SYMBOL,
                        side=OrderSide.BUY,
                        ordertype=OrderType.STOP_MARKET,
                        stopPrice=signal['enter'],
                        newClientOrderId="new_stop",
                        closePosition=True
                    )
                self.client.cancel_order(symbol=SYMBOL, origClientOrderId="stop")
            except Exception:
                return False

            return True


if __name__ == "__main__":
    client = Client()
    import ipdb
    ipdb.set_trace()