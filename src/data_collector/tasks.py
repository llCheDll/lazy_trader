from __future__ import absolute_import, unicode_literals

import json

from asgiref.sync import async_to_sync
from celery import shared_task
from celery.task import periodic_task
from celery.schedules import crontab
from channels.layers import get_channel_layer
from .data_collector import DataCollector


@shared_task
def data_collect(dc_object):
    print("Starting get signal")
    dc_object.get_data()
    signal = dc_object.get_signal()
    if signal:
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            "trade_event",
            {
                "type": "chat_message",
                "message": signal
            }
        )
        print(signal)
    print("Ending get signal\n *******")


@periodic_task(run_every=crontab(minute=1, hour="*/1"), name="data_collect")
def get_data():
    with open("config/coins.json", "r") as file:
        coins_data = json.loads(file.read())
        flag = 0
        for coin in coins_data:
            if "pricePrecision" not in coin["parameters"]:
                with open("config/precisions.json", "r") as file_precision:
                    precisions = json.loads(file_precision.read())
                    coin["parameters"]["pricePrecision"] = precisions[coin["symbol"]]
                    flag += 1
        if flag:
            with open("config/coins.json", "w") as update_data:
                update_data.write(json.dumps(coins_data, indent=4))

    for coin in coins_data:
        data_collect(DataCollector(coin['symbol'], coin['parameters']))


# @periodic_task(run_every=crontab(minute="*/1"), name="rebase_stop")
def update_stop():
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "trade_event",
        {
            "type": "chat_message",
            "message": "rebase_stop"
        }
    )

