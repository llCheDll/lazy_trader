import pandas as pd
import requests
import json

from config import endpoints


class DataCollector:
    def __init__(self, coin_name, coin_data):
        self.coin_name = coin_name
        self.coin_data = coin_data

        self.period_ema_fast = self.coin_data['ema_fast']
        self.period_ema_slow = self.coin_data['ema_slow']
        self.period_atr = self.coin_data['atr']

        self.df = None

    def get_data(self):
        url = endpoints.BASE + \
              endpoints.KLINES + \
              endpoints.KLINE_HISTORY_PARAM.format(
                  self.coin_name, self.coin_data['interval'], self.coin_data['limit']
              )

        data = requests.get(url)
        dictionary = json.loads(data.text)

        column_names = ["time", "open", "high", "low", "close", "volume", "time_close"]
        self.df = pd.DataFrame.from_dict(dictionary)
        self.df.drop(columns=range(7, 12), inplace=True)
        self.df.columns = column_names

        # Clear data
        for column in column_names[1:]:
            self.df[column] = self.df[column].astype(float)

        # self.df = self.df[:self.coin_data["limit"] - 1]
        self.df = self.df

        self.add_ema()
        self.add_atr()

    # Add indicators to data frame
    def add_ema(self):
        self.df["EMA_FAST"] = self.df['close'].ewm(
            span=self.period_ema_fast, min_periods=0, adjust=False, ignore_na=False
        ).mean()
        self.df["EMA_SLOW"] = self.df['close'].ewm(
            span=self.period_ema_slow, min_periods=0, adjust=False, ignore_na=False
        ).mean()

    def add_atr(self):
        ind = range(0, len(self.df))
        index_list = list(ind)
        self.df.index = index_list

        true_range = []

        for index, row in self.df.iterrows():
            max_tr = 0
            if index != 0:
                tr1 = row['high'] - row['low']
                tr2 = abs(row['high'] - self.df.iloc[index-1]['close'])
                tr3 = abs(row['high'] - self.df.iloc[index-1]['close'])
                max_tr = max(tr1, tr2, tr3)
            true_range.append(max_tr)

        self.df["tr"] = true_range
        self.df["ATR"] = self.df["tr"].rolling(
            min_periods=self.period_atr, window=self.period_atr, center=False
        ).mean()

    def coin_precision(self, price, precision):
        return "{price:0.{precision}f}".format(
            price=price, precision=precision
        )

    def get_signal(self):
        row = self.df[:self.coin_data["limit"]-1].tail(1)
        prev_row = self.df[:self.coin_data["limit"]-2].tail(1)

        row = next(row.iterrows())
        prev_row = next(prev_row.iterrows())
        data = row[1]
        prev_data = prev_row[1]

        # long position signal
        ak_trend = (data.EMA_FAST-data.EMA_SLOW)*1.001
        prev_ak_trend = (prev_data.EMA_FAST-prev_data.EMA_SLOW)*1.001

        if ak_trend > 0 and prev_ak_trend < 0:
            enter = self.coin_precision(
                data.close, self.coin_data["pricePrecision"]
            )
            # take = self.coin_precision(
            #     data.close * (1+self.coin_data["take"]), self.coin_data["pricePrecision"]
            # )
            # stop = self.coin_precision(
            #     data.close * (1 - self.coin_data["stop"]), self.coin_data["pricePrecision"]
            # )

            signal = {
                "symbol": self.coin_name,
                "time": data.time,
                "enter": enter,
                "take": self.coin_data["take"],
                "stop": self.coin_data['stop'],
                "precision": self.coin_data["pricePrecision"],
                "trailing": self.coin_data["trailing_callback"],
                "trade": 'long',
                "leverage": self.coin_data["leverage"],
                "margin_type": self.coin_data["margin_type"],
                "quantity": self.coin_data["quantity"],
            }
            return signal

        # short position signal
        elif ak_trend < 0 and prev_ak_trend > 0:
            enter = self.coin_precision(
                data.close, self.coin_data["pricePrecision"]
            )
            # take = self.coin_precision(
            #     data.close * (1 - self.coin_data["take"]), self.coin_data["pricePrecision"]
            # )
            # stop = self.coin_precision(
            #     data.close+data.ATR*self.coin_data['stop'], self.coin_data["pricePrecision"]
            # )

            signal = {
                "symbol": self.coin_name,
                "time": data.time,
                "enter": enter,
                "take": self.coin_data["take"],
                "stop": self.coin_data['stop'],
                "precision": self.coin_data["pricePrecision"],
                "trailing": self.coin_data["trailing_callback"],
                "trade": 'short',
                "leverage": self.coin_data["leverage"],
                "margin_type": self.coin_data["margin_type"],
                "quantity": self.coin_data["quantity"],
            }

            return signal

        return False

    def get_signal_price_channel(self):
        row = self.df[:self.coin_data["limit"]-1].tail(1)

        row = next(row.iterrows())
        data = row[1]

        # long position signal
        if data.high >= data.pc_upper and data.K < self.coin_data['K_long'] and data.close >= data.MA54:
            first_take = self.coin_precision(
                data.close * (1+self.coin_data["take"]), self.coin_data["pricePrecision"]
            )
            second_take = self.coin_precision(
                data.close * (1+self.coin_data["second_take"]), self.coin_data["pricePrecision"]
            )
            stop = self.coin_precision(
                data.close * (1 - self.coin_data["stop"]), self.coin_data["pricePrecision"]
            )
            enter = self.coin_precision(
                data.close*1.02, self.coin_data["pricePrecision"]
            )

            signal = {
                "symbol": self.coin_name,
                "time": data.time,
                "first_take": first_take,
                "second_take": second_take,
                "enter": enter,
                "stop": stop,
                "trade": 'long',
                "leverage": self.coin_data["leverage"],
                "margin_type": self.coin_data["margin_type"],
                "quantity": self.coin_data["quantity"],
                "first_trade_value": self.coin_data["first_trade_value"],
                "second_trade_value": self.coin_data["second_trade_value"]
            }
            return signal

        # short position signal
        elif data.low <= data.pc_lower and data.K > self.coin_data['K_short'] and data.close <= data.MA54:
            first_take = self.coin_precision(
                data.close * (1 - self.coin_data["first_take"]), self.coin_data["pricePrecision"]
            )

            second_take = self.coin_precision(
                data.close * (1 - self.coin_data["second_take"]), self.coin_data["pricePrecision"]
            )
            stop = self.coin_precision(
                data.close * (1 + self.coin_data["stop"]), self.coin_data["pricePrecision"]
            )
            enter = self.coin_precision(
                data.close*0.98, self.coin_data["pricePrecision"]
            )

            signal = {
                "symbol": self.coin_name,
                "time": data.time,
                "first_take": first_take,
                "second_take": second_take,
                "enter": enter,
                "stop": stop,
                "trade": 'short',
                "leverage": self.coin_data["leverage"],
                "margin_type": self.coin_data["margin_type"],
                "quantity": self.coin_data["quantity"],
                "first_trade_value": self.coin_data["first_trade_value"],
                "second_trade_value": self.coin_data["second_trade_value"]
            }

            return signal

        return False
