from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('trade/', views.room, name='room'),
]
