import pandas as pd
import requests
import json

from src.config import endpoints
from datetime import datetime


class DataCollector:
    def __init__(self):
        self.period_pc = endpoints.PRICE_CHANNEL_PERIOD
        self.period_stoch = endpoints.STOCH_PERIOD
        self.df = None
        self.history = pd.DataFrame()

    def get_data(self):
        url = endpoints.BASE + \
              endpoints.KLINES + \
              endpoints.KLINE_HISTORY_PARAM

        data = requests.get(url)
        dictionary = json.loads(data.text)

        column_names = ["time", "open", "high", "low", "close", "volume", "time_close"]
        self.df = pd.DataFrame.from_dict(dictionary)
        self.df.drop(columns=range(7, 12), inplace=True)
        self.df.columns = column_names

        #Clear data
        for column in column_names[1:]:
            self.df[column] = self.df[column].astype(float)

        # self.df = self.df[:endpoints.LIMIT - 1]
        self.df = self.df

        self.add_price_channel()
        self.add_stoch()
        self.add_ma(endpoints.MA)

    def get_data_history(self, time_start):
        url = endpoints.BASE + \
              endpoints.KLINES + \
              endpoints.KLINE_HISTORY_PARAM_BIG_DATA.format(
                  endpoints.SYMBOL,
                  endpoints.INTERVAL,
                  time_start,
                  endpoints.LIMIT
              )

        data = requests.get(url)
        dictionary = json.loads(data.text)

        hist = pd.DataFrame.from_dict(dictionary)
        self.history = self.history.append(other=hist[1:], ignore_index=True)

    def get_old_hist(self):
        counter = 30
        dt_obj = datetime.strptime('01.07.2020 00:00:00,00',
                                   '%d.%m.%Y %H:%M:%S,%f')
        millisec = int(dt_obj.timestamp() * 1000)

        for i in range(0, counter):
            if self.history.empty:
                self.get_data_history(millisec)
            self.get_data_history(self.history[0][len(self.history)-1])

        column_names = ["time", "open", "high", "low", "close", "volume", "time_close"]
        self.history.drop(columns=range(7, 12), inplace=True)
        self.history.columns = column_names

        #Clear data
        for column in column_names[1:]:
            self.history[column] = self.history[column].astype(float)

        self.add_price_channel_hist()
        self.add_stoch_hist()
        self.add_ma_hist(endpoints.MA)

    def get_last_closed_kline(self):
        return self.df[0:-1]

    def get_current_price(self):
        url = endpoints.BASE + endpoints.CURRENT_PRICE

        data = requests.get(url)
        dictionary = json.loads(data.text)

        return dictionary

    #Add indicators to data frame
    def add_price_channel(self):
        self.df['pc_upper'] = self.df['high'].rolling(window=self.period_pc).max()
        self.df['pc_lower'] = self.df['low'].rolling(window=self.period_pc).min()

    def add_stoch(self):
        # Create the "L14" column in the DataFrame
        self.df['L'] = self.df['low'].rolling(window=self.period_stoch).min()

        # Create the "H14" column in the DataFrame
        self.df['H'] = self.df['high'].rolling(window=self.period_stoch).max()

        # Create the "%K" column in the DataFrame
        self.df['K'] = 100 * ((self.df['close'] - self.df['L']) / (self.df['H'] - self.df['L']))

        # Create the "%D" column in the DataFrame
        self.df['D'] = self.df['K'].rolling(window=3).mean()

    def add_ma(self, period):
        ma = "MA" + str(period)
        self.df[ma] = self.df['close'].rolling(period).mean()

    def add_price_channel_hist(self):
        self.history['pc_upper'] = self.history['high'].rolling(window=self.period_pc).max()
        self.history['pc_lower'] = self.history['low'].rolling(window=self.period_pc).min()

    def add_stoch_hist(self):
        # Create the "L14" column in the DataFrame
        self.history['L'] = self.history['low'].rolling(window=self.period_stoch).min()

        # Create the "H14" column in the DataFrame
        self.history['H'] = self.history['high'].rolling(window=self.period_stoch).max()

        # Create the "%K" column in the DataFrame
        self.history['K'] = 100 * ((self.history['close'] - self.history['L']) / (self.history['H'] - self.history['L']))

        # Create the "%D" column in the DataFrame
        self.history['D'] = self.history['K'].rolling(window=3).mean()

    def add_ma_hist(self, period):
        ma = "MA" + str(period)
        self.history[ma] = self.history['close'].rolling(period).mean()


if __name__ == '__main__':
    dt = DataCollector()
    dt.get_old_hist()
